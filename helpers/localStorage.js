export const setLocalStore = (token) => localStorage.setItem("token", token)

export const getLocalStore = () => localStorage.getItem("token")

export const removeLocalStore = () => localStorage.removeItem("token")
