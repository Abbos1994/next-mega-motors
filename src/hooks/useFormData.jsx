// src/hooks/useFormData.js
import { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"

export const useFormData = (selector, initialData = {}, fetchAction, id) => {
  const dispatch = useDispatch()
  const data = useSelector(selector)
  const [formData, setFormData] = useState(initialData)

  useEffect(() => {
    if (id && fetchAction) {
      dispatch(fetchAction(id))
    }
  }, [id, dispatch, fetchAction])

  useEffect(() => {
    if (data) {
      setFormData(data)
    }
  }, [data])

  return [formData, setFormData]
}
