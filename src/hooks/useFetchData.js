//src/hooks/useFetchData
import { useEffect } from "react"
import { useDispatch } from "react-redux"

export const useFetchData = (action, deps = []) => {
  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(action())
  }, [dispatch, ...deps])
}
