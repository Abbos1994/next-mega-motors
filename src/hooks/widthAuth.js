import React, { useEffect } from "react"
import { useRouter } from "next/navigation"
import Cookies from "js-cookie"

export default function withAuth(Component) {
  const c = Cookies(Component)
  console.log(c)
  return function WithAuth(props) {
    const session = true
    const { push } = useRouter()

    useEffect(() => {
      if (!session) {
        push("/login")
      }
    }, [session])

    if (!session) {
      return null
    }

    return <Component {...props} />
  }
}
