import { Schema, models, model } from "mongoose"

const schema = new Schema({
  title: { type: String, required: true },
  img: { type: Schema.Types.ObjectId, ref: "File" },
})

export default models.Discount || model("Discount", schema)
