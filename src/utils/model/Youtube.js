import { Schema, models, model } from "mongoose"

const schema = new Schema({
  link: { type: String, required: true },
})

export default models.Youtube || model("Youtube", schema)
