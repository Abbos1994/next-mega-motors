import { Schema, models, model } from "mongoose"

const schema = new Schema({
  path: String,
  name: String,
  type: String,
  size: Number,
})

export default models.File || model("File", schema)
