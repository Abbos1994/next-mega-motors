import { Schema, models, model } from "mongoose"

const schema = new Schema({
  modelId: { type: Schema.Types.ObjectId, ref: "Model", required: true },
})

export default models.Slider || model("Slider", schema)
