import { Schema, models, model } from "mongoose"

const schema = new Schema({
  name: String,
  tel: Number,
  text: String,
})

export default models.Brand || model("Brand", schema)
