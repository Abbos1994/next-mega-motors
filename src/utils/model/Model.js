import { Schema, models, model } from "mongoose"

const schema = new Schema({
  title: { type: String, required: true },
  description: { type: String, required: true },
  price: { type: Number, required: true },
  text: { type: String },
  favorite: { type: Boolean },
  img: { type: Schema.Types.ObjectId, ref: "File", required: false },
  brand: { type: Schema.Types.ObjectId, ref: "Brand", required: true },
})

export default models.Model || model("Model", schema)
