import path from "path"
import fs from "fs/promises"

const deleteFileFromServer = async (file) => {
  try {
    const fullPath = path.join(process.cwd(), "public", file)
    await fs.unlink(fullPath)
  } catch (e) {
    throw new Error(e.message)
  }
}
export default deleteFileFromServer
