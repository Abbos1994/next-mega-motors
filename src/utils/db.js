import mongoose from "mongoose"

async function db() {
  await mongoose
    .connect("mongodb://0.0.0.0:27017/megamotors")
    .then(() => console.log("DB connected"))
    .catch((e) => console.error("Error connecting: " + e.message))
}
export default db
