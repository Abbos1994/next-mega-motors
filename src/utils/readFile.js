import path from "path"
import crypto from "crypto"
import formidable from "formidable"

const readFile = async (req) => {
  const options = {
    uploadDir: path.join(process.cwd(), "/public/img"),
    filename: (name, ext, path) => {
      const afterLastSlash = path.originalFilename.substring(
        path.originalFilename.lastIndexOf("/") + 1
      )
      const afterLastDot = afterLastSlash.substring(
        afterLastSlash.lastIndexOf(".") + 1
      )
      const hash = crypto
        .createHash("md5")
        .update(path.originalFilename)
        .digest("hex")
      return `${Date.now()}_${hash}.${afterLastDot}`
    },
  }

  const form = formidable(options)
  return new Promise((resolve, reject) => {
    form.parse(req, (err, fields, files) => {
      if (err) reject(err)
      resolve({ fields, files })
    })
  })
}
export default readFile
