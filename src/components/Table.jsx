// src/components/Table.js
"use client"
import React, { useState, useEffect } from "react"
import Image from "next/image"
import Link from "next/link"
import { toast } from "react-toastify"
import { usePathname } from "next/navigation"
import { useRouter } from "next/router"

function Table({ states, url }) {
  const p = usePathname()
  const router = useRouter()
  const keys = Object?.keys(states[0] || {})

  const [img, setImg] = useState([])

  useEffect(() => {
    const getImages = async () => {
        const response = await fetch(`/api/file`)
        const images = await response.json()
        setImg(images)
    }

    getImages().catch((e) => toast.error(e.message))
  }, [])

  const handleRemoveClick = async (id) => {
    try {
      const r = await fetch(`/api/${url}?id=${id}`, {
        method: "DELETE",
      })
      const { message } = await r.json()
      if (r.ok) {
        await toast.success(message || "O'chirildi!")
        await router.reload()
      } else {
        toast.error(message || "Xatolik!")
      }
    } catch (e) {
      toast.error(e.message || "Xatolik!")
    }
  }

  const renderCellContent = (state, key) => {
    if (key === "_id") return null

    if (key === "path") {
      return (
        <div className="h-16 w-16 relative cursor-pointer m-auto">
          <Image
            src={state[key]}
            alt={state[key]}
            fill
            sizes="(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw"
          />
        </div>
      )
    } else if (key === "img") {
      const matchingImage = img.find(({ _id }) => _id === state[key])
      if (matchingImage) {
        return (
          <div className="h-16 w-16 relative cursor-pointer m-auto">
            <Image
              src={matchingImage.path}
              alt={matchingImage.name}
              fill
              priority
              sizes="(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw"
            />
          </div>
        )
      }
    } else {
      return state[key]
    }
  }

  return (
    <div className="overflow-x-auto">
      <table className="min-w-full table-auto">
        <thead>
          <tr>
            {keys.map((key) => {
              if (key !== "_id") {
                return (
                  <th
                    key={key}
                    className="p-3 font-bold uppercase bg-gray-700 text-gray-100 border border-gray-300 hidden lg:table-cell"
                  >
                    {key}
                  </th>
                )
              }
              return null
            })}
            <th className="p-3 font-bold uppercase bg-gray-700 text-green-400 border border-gray-300 hidden lg:table-cell">
              <Link href={`${p}/create?url=${url}`}>Create</Link>
            </th>
          </tr>
        </thead>
        <tbody>
          {states?.map((state, index) => (
            <tr
              key={index}
              className="bg-gray-700 lg:hover:bg-gray-600 flex lg:table-row flex-row lg:flex-row flex-wrap lg:flex-no-wrap mb-10 lg:mb-0"
            >
              {keys.map((key) => {
                if (key !== "_id") {
                  return (
                    <td
                      key={key}
                      className="w-full lg:w-auto p-3 text-gray-100 text-center border border-b block lg:table-cell relative lg:static"
                    >
                      <span className="lg:hidden bg-white text-black text-xs font-bold uppercase absolute top-[-2px] px-2 left-52">
                        {key}
                      </span>
                      {renderCellContent(state, key)}
                    </td>
                  )
                }
                return null
              })}
              <td className="w-full lg:w-auto p-3 text-gray-800 text-center border border-b text-center block lg:table-cell relative lg:static">
                <span className="lg:hidden text-green-400 hover:text-green-600 hover:underline font-bold uppercase p-3">
                  <Link href={`${p}/create?url=${url}`}>Create</Link>
                </span>
                <Link
                  href={`${p}/create?id=${state._id}&url=${url}`}
                  className="text-yellow-300 hover:underline hover:text-yellow-600 p-3"
                >
                  Edit
                </Link>
                <button
                  onClick={() => handleRemoveClick(state._id)}
                  className="text-red-300 hover:underline hover:text-red-600 p-3"
                >
                  Remove
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  )
}

export default Table
