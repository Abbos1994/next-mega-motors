//src/components/header.js
import React, { useState, useEffect } from "react"
import Image from "next/image"
import Link from "next/link"

import { ArrowLeftIcon, ArrowRightIcon } from "../../public/Icons/Icon"

export default function Header() {
  const [currentImageIndex, setCurrentImageIndex] = useState(0)
  const images = [
    "/images/fon1.jpg",
    "/images/teslarasm.png",
    "/images/BYD.png",
  ]

  useEffect(() => {
    const autoChangeInterval = setInterval(() => {
      nextImage()
    }, 3000)

    return () => clearInterval(autoChangeInterval)
  }, [currentImageIndex])

  const nextImage = () => {
    setCurrentImageIndex((prevIndex) => (prevIndex + 1) % images.length)
  }

  const prevImage = () => {
    setCurrentImageIndex(
      (prevIndex) => (prevIndex - 1 + images.length) % images.length
    )
  }

  return (
    <div>
      <div className="container mx-auto">
        <div className="flex flex-col justify-center md:h-[400px] xl:h-height ">
          <div className=" flex h-60  justify-around md:h-[400px] xl:h-height ">
            <div className="">
              <button className="h-full w-full" onClick={prevImage}>
                <ArrowLeftIcon />
              </button>
            </div>

            <div className="relative w-3/4 overflow-hidden">
              {images.map((src, index) => (
                <Image
                  width={1000}
                  height={1000}
                  key={index}
                  className={`absolute left-1/2 h-[90%] w-11/12 -translate-x-1/2 transform ${
                    index === currentImageIndex ? "opacity-100" : "opacity-0"
                  } transition-opacity duration-1000`}
                  src={src}
                  alt={`Slayder ${index}`}
                />
              ))}
            </div>
            <button className="" onClick={nextImage}>
              <ArrowRightIcon />
            </button>
          </div>

          <button className="textshadow mb-5 font-bold tracking-widest md:mb-8 md:text-2xl xl:mb-12 xl:text-5xl">
            Подробнее
          </button>
        </div>
      </div>
      {/* Kurgovoy slayder menyu */}
      <div className="container mx-auto">
        <h1 className="fond-bold pt-8 text-center text-xl md:text-3xl xl:text-6xl ">
          Brend
        </h1>
        <div className="flex h-44 items-center justify-evenly md:h-96 xl:h-[500px] ">
          <div className="flex h-3/5 w-[19%] items-center justify-center rounded-full border-2 border-solid border-blue-600 shadow">
            <Link href="">
              <Image
                width={1000}
                height={1000}
                src="/images/teslaa.png"
                alt="Brend"
              />
            </Link>
          </div>
          <div className="flex h-3/5 w-[19%] items-center justify-center rounded-full border-2 border-solid border-blue-600 shadow">
            <Link href="">
              <Image
                width={1000}
                height={1000}
                src="/images/BYD.png"
                alt="Brend"
              />
            </Link>
          </div>
          <div className="flex h-3/5 w-[19%] items-center justify-center rounded-full border-2 border-solid border-blue-600 shadow">
            <Link href="">
              <Image
                width={1000}
                height={1000}
                src="/images/audi.png"
                alt="Brend"
              />
            </Link>
          </div>
          <div className="flex h-3/5 w-[19%] items-center justify-center rounded-full border-2 border-solid border-blue-600 shadow">
            <Link href="#">
              <Image
                width={1000}
                height={1000}
                src="/images/xpeng.png"
                alt="Brend"
              />
            </Link>
          </div>
          <div className="flex h-3/5 w-[19%] items-center justify-center rounded-full border-2 border-solid border-blue-600 shadow">
            <Link href="#">
              <Image
                width={1000}
                height={1000}
                src={"/images/nio.png"}
                alt="Brend"
              />
            </Link>
          </div>
        </div>
      </div>
      {/* Kurgovoy slayder menyu => model */}
      <div className="container mx-auto flex h-height items-center justify-center">
        <div className="mx-4 flex h-[430px] w-11/12 max-w-[500px] rounded-3xl shadow md:h-[600px] ">
          <div className=" flex w-full flex-col items-center gap-y-10 rounded-3xl ">
            <Image
              width={1000}
              height={1000}
              className="mt-3 w-full "
              src="/images/teslarasm.png"
              alt="brand"
            />

            <h1 className="md:text:xl text-center text-xl font-medium xl:text-3xl">
              Tesla Model X
            </h1>
            <Link
              href="#"
              className="fond-bold shadoww mx-auto rounded bg-slate-500 px-8 py-1 text-xl text-white md:px-12 md:py-2 xl:px-16 xl:py-3"
            >
              Malumot
            </Link>
          </div>
        </div>
      </div>
      {/* Наши премушества menyu */}
      <div className=" h-[650px] xl:h-height">
        <div className="mx-auto h-4/5 w-11/12 max-w-[900px] items-center rounded-3xl shadow xl:flex">
          <Image
            width={1000}
            height={1000}
            className="mx-auto "
            src="/images/teslarasm.png"
            alt="img"
          />
          <div className=" flex  flex-col items-center justify-center ">
            <h1 className="text-bold text-center text-xl md:text-3xl xl:text-4xl">
              Header text
            </h1>
            <p className="mx-auto mt-4 w-4/5 text-justify md:text-lg xl:text-2xl">
              Lorem ipsum dolor sit, amet consectetur adipisicing elit. Non
              voluptatum, voluptas fuga unde vel quisquam nemo earum aspernatur
              quaerat ea?
            </p>
          </div>
        </div>
      </div>
      {/* Youtube kurgovoy slayder */}
      <div className="flex h-96 items-center justify-center ">
        <div className="shadoww h-64 w-64 rounded-full border-4">
          <Image
            width={1000}
            height={1000}
            className="rounded-full "
            src="/images/youtube.jpg"
            alt="youtube"
          />
        </div>
      </div>
      {/* Forma menyu */}
      <div className="h-64">
        <form className="mx-auto w-11/12 max-w-lg justify-around xl:flex xl:max-w-4xl">
          <div className="xl:w-2/5">
            <label htmlFor="nameform">Name</label>
            <input
              className="w-full rounded-md border-2 py-2 ps-2 text-lg"
              type="text"
              id="nameform"
              placeholder="Enter name"
            />
          </div>
          <div className="xl:w-2/5">
            <label htmlFor="telform">Tel</label>
            <input
              className=" w-full rounded-md border-2 py-2 ps-2 text-lg"
              type="tel"
              id="telform"
              placeholder="Enter telefon"
            />
          </div>
        </form>
      </div>
    </div>
  )
}
