import React from "react"
import Image from "next/image"
import Link from "next/link"

function Navbar() {
  return (
    <div>
      <div className="container mx-auto">
        <nav className="flex h-16 justify-center">
          <ul className="flex  w-3/5 items-center justify-between">
            <li>
              <Link href={"/models"} className="font-medium">
                Все модели
              </Link>
            </li>
            <li>
              <Link href="#" className="font-medium">
                Грантия и Сервис
              </Link>
              -[20%]{" "}
            </li>
            <li>
              <Link href="#" className="font-medium">
                О компании
              </Link>
            </li>
            <li>
              <Link href="#" className="font-medium">
                Акции
              </Link>
            </li>
            <li>
              <Link href="#" className="font-medium">
                Жалобы и вопросы
              </Link>
            </li>
          </ul>
        </nav>
        {/*Все модели menyu */}
        <div className="flex  h-96 w-full flex-wrap items-center bg-white">
          <div className="flex w-full flex-wrap justify-around xl:w-1/2">
            <div className=" h-auto max-h-[20%] w-auto min-w-[120px] max-w-[160px]">
              <Image
                width={1000}
                height={1000}
                className="mx-auto w-20 md:w-full"
                src={"/images/teslaa.png"}
                alt="Rasm topilmadi"
              />
              <h1 className="text-center text-xl font-bold text-black">
                Tesla
              </h1>
            </div>
            <div className=" h-auto max-h-[20%] w-auto min-w-[120px] max-w-[160px]  ">
              <Image
                width={1000}
                height={1000}
                className="mx-auto w-20 md:w-full"
                src="/images/BYD.png"
                alt="Rasm topilmadi"
              />
              <h1 className="text-center text-xl font-bold text-black">BYD</h1>
            </div>
            <div className=" h-auto max-h-[20%] w-auto min-w-[120px] max-w-[160px]  ">
              <Image
                width={1000}
                height={1000}
                className="mx-auto w-20 md:w-full"
                src="/images/audi.png"
                alt="Rasm topilmadi"
              />
              <h1 className="text-center text-xl font-bold text-black">Audi</h1>
            </div>
          </div>
          <div className="flex w-full flex-wrap justify-around xl:w-1/2">
            <div className=" h-auto max-h-[20%] w-auto min-w-[120px] max-w-[160px]  ">
              <Image
                width={1000}
                height={1000}
                className="mx-auto w-20 md:w-full"
                src="/images/xpeng.png"
                alt="Rasm topilmadi"
              />
              <h1 className="text-center text-xl font-bold text-black">
                XPeng
              </h1>
            </div>
            <div className=" h-auto max-h-[20%] w-auto min-w-[120px] max-w-[160px]">
              <Image
                width={1000}
                height={1000}
                className="mx-auto  w-20 md:w-full"
                src="/images/vw.png"
                alt="Rasm topilmadi"
              />
              <h1 className="text-center text-xl font-bold text-black">
                Volkswagen
              </h1>
            </div>
            <div className=" h-auto max-h-[20%] w-auto min-w-[120px] max-w-[160px] ">
              <Image
                width={1000}
                height={1000}
                className="mx-auto  w-20 md:w-full"
                src="/images/nio.png"
                alt="Rasm topilmadi"
              />
              <h1 className="text-center text-xl font-bold text-black">Nio </h1>
            </div>
          </div>
        </div>
        {/*Все модели ==> brend menyu */}
        <div className="h-[800px] bg-gradient-to-r from-cyan-500 py-1  md:h-[900px] xl:flex xl:h-height ">
          <div className="m-5 flex w-11/12 justify-center rounded-lg bg-slate-50 xl:w-[70%] xl:rounded-[25px]">
            <Image
              width={1000}
              height={1000}
              className=" w-full xl:rounded-[25px]"
              src="/images/cars.png"
              alt="#"
            />
          </div>
          <div className="flex  items-center justify-center xl:w-[27%]">
            <div className="flex w-11/12 flex-col gap-y-3 md:w-1/2 xl:w-11/12">
              <h1 className="text-center text-xl  font-bold tracking-wide text-blue2 md:text-2xl xl:text-3xl">
                Tesla Model X
              </h1>
              <h2 className="text-center text-lg font-bold tracking-wide text-black md:text-2xl xl:text-3xl ">
                40 000$
              </h2>
              <form className="flex flex-col gap-y-4">
                <div>
                  <label className="xl:text-xl " htmlFor="textt">
                    Name
                  </label>
                  <input
                    id="textt"
                    type="text"
                    placeholder="Enter name"
                    className="block  w-full rounded-md border-2 border-solid border-slate-300 py-3 ps-3 xl:text-xl"
                  />
                </div>
                <div>
                  <label className="xl:text-xl" htmlFor="tell">
                    Tel
                  </label>
                  <input
                    id="tell"
                    type="tel"
                    placeholder="Enter telefon"
                    className="block w-full rounded-md border-2 border-solid border-slate-300 py-3 ps-3 xl:text-xl"
                  />
                </div>
                <div className="flex justify-center">
                  <button
                    type="button"
                    className="rounded bg-blue-600 px-2 py-0.5 text-center text-[10px] text-lg font-medium tracking-wide text-white md:px-2  md:text-xl xl:px-4"
                  >
                    Отправит
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
        {/* Грантия и Сервис menyu */}
        <div className="max-h-height flex h-auto items-center justify-center py-20">
          <div className="mx-auto flex w-1/2 items-center xl:py-32">
            <Image
              width={1000}
              height={1000}
              className="h-auto w-full md:w-auto xl:h-3/4 xl:w-full"
              src="/images/teslarasm.png"
              alt="#"
            />
          </div>
          <div className=" w-1/2 justify-around md:flex md:flex-col">
            <h1 className="text-center text-xl font-bold text-blue-400 xl:text-5xl">
              Our Services
            </h1>
            <div className="flex-col gap-y-3 xl:flex">
              <div className="mt-4 flex items-center gap-x-3 xl:gap-x-5">
                <div className="h-1/3 w-16 md:h-12 md:w-16 xl:h-20 xl:w-24">
                  <Image
                    width={1000}
                    height={1000}
                    className="h-full w-full"
                    src="/images/Vector.png"
                    alt="#"
                  />
                </div>
                <div className="w-11/12 xl:w-1/2">
                  <h2 className="text-sm font-bold md:text-xl xl:text-2xl">
                    Car Hire
                  </h2>
                  <p className="text-[10px] md:text-sm xl:text-xl">
                    We pride ourselves in always going the extra mile for our
                    customers.
                  </p>
                </div>
              </div>
              <div className="mt-4 flex items-center gap-x-3 xl:gap-x-5">
                <div className="h-1/3 w-16 md:h-12 md:w-16 xl:h-20 xl:w-24">
                  <Image
                    width={1000}
                    height={1000}
                    className="h-full w-full"
                    src="/images/Vector.png"
                    alt="#"
                  />
                </div>
                <div className="w-11/12 xl:w-1/2">
                  <h2 className="text-sm font-bold md:text-xl xl:text-2xl">
                    Car Hire
                  </h2>
                  <p className="text-[10px] md:text-sm  xl:text-xl">
                    We pride ourselves in always going the extra mile for our
                    customers.
                  </p>
                </div>
              </div>
              <div className="mt-4 flex items-center gap-x-3 xl:gap-x-5">
                <div className="h-1/3 w-16 md:h-12 md:w-16 xl:h-20 xl:w-24">
                  <Image
                    width={1000}
                    height={1000}
                    className="h-full w-full"
                    src="/images/Vector.png"
                    alt="#"
                  />
                </div>
                <div className="w-11/12 xl:w-1/2">
                  <h2 className="text-sm font-bold md:text-xl xl:text-2xl">
                    Car Hire
                  </h2>
                  <p className="text-[10px] md:text-sm xl:text-xl">
                    We pride ourselves in always going the extra mile for our
                    customers.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* О компании menyu */}
        <div className="h-[480px] w-full md:h-[715px] xl:flex  xl:items-center ">
          <div className="text-center xl:w-1/2">
            <h1 className="text-center text-2xl font-bold  leading-loose text-cyan-500 xl:ms-10 xl:text-5xl ">
              О нас
            </h1>
            <h4 className="mx-5 mt-2 text-center xl:float-right xl:mx-0 xl:mt-6 xl:w-[85%] xl:text-lg">
              <span className="font-bold xl:text-xl">MEGA MOTORS</span> -
              является уникальным проектом по продаже экологически чистого
              электро-автотранспорта. Среди продавцов электромобилей мы являемся
              единственным поставщиком с широким ассортиментом электромобилей в
              наличии.
            </h4>
          </div>
          <div className="mt-10 flex justify-center xl:w-1/2">
            <Image width={1000} height={1000} src="/images/Mlogo.png" />
          </div>
        </div>
        {/* Акции menyu */}
        <div className="max-h-height flex  justify-center py-4">
          <div>
            <h1 className="mx-auto w-4/5 text-center text-xl font-bold text-blue2 md:text-3xl xl:text-5xl">
              Aksiya
            </h1>
            <Image
              width={1000}
              height={1000}
              className="mx-auto w-4/5 "
              src="/images/lambarghini.png"
              alt="#"
            />
          </div>
        </div>
        {/* Жалобы и вопросы menyu */}
        <div>
          <div className="flex h-height w-full items-center justify-center md:mx-0">
            <form className=" flex w-11/12 flex-col gap-y-5 md:w-1/2 xl:w-1/3">
              <div>
                <label className=" text-xl xl:text-2xl" htmlFor="text">
                  Text
                </label>
                <input
                  id="text"
                  type="text"
                  placeholder="Enter text"
                  className="block w-full rounded-md border-2 border-solid border-slate-300 py-4 ps-3 text-xl xl:text-2xl"
                />
              </div>
              <div>
                <label className=" text-xl xl:text-2xl" htmlFor="name">
                  Name
                </label>
                <input
                  id="name"
                  type="text"
                  placeholder="Enter name"
                  className="block w-full rounded-md border-2 border-solid border-slate-300 py-4 ps-3 text-xl xl:text-2xl"
                />
              </div>
              <div>
                <label className=" text-xl xl:text-2xl" htmlFor="tel">
                  Tel
                </label>
                <input
                  id="tel"
                  type="tel"
                  placeholder="Enter telefon"
                  className="block w-full rounded-md border-2 border-solid border-slate-300 py-4 ps-3 text-xl xl:text-2xl "
                />
              </div>
              <button className="mt-4 w-full  rounded-md border-2 border-solid bg-blue-600 py-4 font-medium text-white md:text-lg xl:text-xl ">
                Отправит
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Navbar
