// src/actions/uploadActions.js
import { genericAction } from "@/helpers/action"

const T = "FILE"

export const fetchUploads = () => genericAction(T + "S", "FETCH")
export const fetchUpload = (id, data) => genericAction(T, "FETCH", data, id)
export const createUpload = (data) => genericAction(T, "CREATE_UPLOAD", data)
export const updateUpload = (id, data) => genericAction(T, "UPDATE", data, id)
export const deleteUpload = (id) => genericAction(T, "DELETE", null, id)
