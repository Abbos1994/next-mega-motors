import { genericAction } from "@/helpers/action"

const T = "LINK"

export const fetchYoutubes = () => genericAction(T + "S", "FETCH")
export const fetchYoutube = (id) => genericAction(T, "FETCH", null, id)
export const createYoutube = (data) => genericAction(T, "CREATE", data)
export const updateYoutube = (id, data) => genericAction(T, "UPDATE", data, id)
export const deleteYoutube = (id) => genericAction(T, "DELETE", null, id)
