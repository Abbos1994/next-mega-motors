// src/actions/youtubeActions.js
import { genericAction } from "@/helpers/action"

const T = "DISCOUNT"

export const fetchDiscount = (id) => genericAction(T, "FETCH", null, id)
export const fetchDiscounts = () => genericAction(T + "S", "FETCH")
export const createDiscount = (data) => genericAction(T, "CREATE", data, null)
export const updateDiscount = (id, data) => genericAction(T, "UPDATE", data, id)
export const deleteDiscount = (id) => genericAction(T, "DELETE", null, id)
