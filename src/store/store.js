//src/store/store.js
import { applyMiddleware, combineReducers, createStore } from "redux"
import thunk from "redux-thunk"

import discountReducer from "./reducers/discountReducer"
import uploadReducer from "./reducers/fileReducer"

const rootReducer = combineReducers({
  discounts: discountReducer,
  uploads: uploadReducer,
})

const store = createStore(rootReducer, applyMiddleware(thunk))

export default store
