// src/reducers/linkReducer.js
import { createInitialState, genericReducer } from "@/helpers/reducer"

const initialState = createInitialState("discount")

const DiscountReducer = (state = initialState, action) => {
  return genericReducer("DISCOUNT", state, action)
}

export default DiscountReducer
