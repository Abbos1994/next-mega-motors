import { createInitialState, genericReducer } from "@/helpers/reducer"

const initialState = createInitialState("uploads")

const uploadsReducer = (state = initialState, action) => {
  return genericReducer("UPLOAD", state, action)
}

export default uploadsReducer
