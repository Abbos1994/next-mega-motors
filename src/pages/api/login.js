// src/pages/api/login.js
import jwt from "jsonwebtoken"
import cookie from "cookie"

export default async function handler(req, res) {
  try {
    if (req.method === "POST") {
      const { username, password } = req.body
      const { NEXT_PASSWORD, NEXT_SECRET, NEXT_LOGIN } = process.env

      if (username === NEXT_LOGIN && password === NEXT_PASSWORD) {
        const token = jwt.sign({ username }, NEXT_SECRET, {
          expiresIn: "1h",
        })
        res.setHeader(
          "Set-Cookie",
          cookie.serialize("token", token, {
            httpOnly: true,
            maxAge: 3600,
            path: "/",
          })
        )
        res.status(200).json({ message: `Muvofaqqiyatli kirildi`, token })
      } else {
        res.status(401).json({ message: `Login yoki parol noto'g'ri` })
      }
    } else {
      res.status(405).json({ message: "Method noto'g'ri" })
    }
  } catch (e) {
    res.status(500).json(e)
  }
}
