// src/pages/api/model
import db from "../../utils/db"
import Model from "../../utils/model/Model"

const handler = async (req, res) => {
  await db()
  try {
    const { brand, id } = await req.query
    switch (req.method) {
      case "GET": {
        try {
          if (id || brand) {
            if (id) {
              const data = await Model.findById(id)
              if (!data) {
                res.status(404).json({ message: "Topilmadi!" })
              } else {
                res.status(200).json(data)
              }
            }
            if (brand) {
              const data = await Model.find({ brand })
              if (!data || data.length === 0) {
                res.status(404).json({ message: "Topilmadi!" })
              } else {
                res.status(200).json(data)
              }
            }
          } else {
            const data = await Model.find()
            res.status(200).json(data)
          }
        } catch (e) {
          res.status(500).json({ message: "Serverda xatolik: " + e.message })
        }
        break
      }
      case "POST": {
        try {
          const newModel = new Model(req.body)
          await newModel.save()
          res.status(201).json(newModel)
        } catch (e) {
          res.status(500).json({ message: "Serverda xatolik: " + e.message })
        }
        break
      }
      case "PUT": {
        try {
          const { id } = req.query
          const updatedModel = await Model.findByIdAndUpdate(id, req.body, {
            new: true,
            runValidators: true,
          })
          if (!updatedModel) {
            res.status(404).json({ message: "Topilmadi!" })
          } else {
            res.status(200).json(updatedModel)
          }
        } catch (e) {
          res.status(500).json({ message: "Serverda xatolik: " + e.message })
        }
        break
      }
      case "DELETE": {
        const { id } = req.query
        if (!id) {
          res.status(400).json({ message: "ID majburiy!" })
        } else {
          try {
            const data = await Model.findById(id)
            if (!data) {
              res.status(404).json({ message: "Topilmadi!" })
            } else {
              await Model.findByIdAndDelete(id)
              res.status(200).json({ message: "O'chirildi!" })
            }
          } catch (e) {
            res.status(500).json({ message: "Serverda xatolik: " + e.message })
          }
        }
        break
      }
      default:
        res.status(405).json({ message: "Method noto'g'ri!" })
    }
  } catch (e) {
    res.status(500).json({ message: "Serverda xatolik: " + e.message })
  }
}

export default handler
