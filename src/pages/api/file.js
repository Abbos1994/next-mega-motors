// src/pages/api/image/index.ts
import db from "../../utils/db"
import File from "../../utils/model/File"
import readFile from "../../utils/readFile"
import deleteFileFromServer from "../../utils/deleteFileFromServer"

export const config = { api: { bodyParser: false } }

const handler = async (req, res) => {
  await db()
  try {
    switch (req.method) {
      case "POST": {
        const { files } = await readFile(req, true)
        const uploadedFile = files["upload"][0]
        const relativePath = `/img/${uploadedFile.newFilename}`
        const newFile = new File({
          name: uploadedFile.newFilename,
          type: uploadedFile.mimetype,
          size: uploadedFile.size,
          path: relativePath,
        })
        await newFile.save()
        res.status(201).json({ ...newFile, message: "Success!" })
        break
      }
      case "GET": {
        const { id } = req.query
        if (id) {
          const file = await File.findById(id)
          res.status(200).json(file)
        } else {
          const files = await File.find()
          res.status(200).json(files)
        }
        break
      }
      case "DELETE": {
        try {
          const { id } = req.query
          if (!id) return res.status(400).json({ message: "Fayl ID Majburiy!" })
          const fileToDelete = await File.findById(id)
          if (!fileToDelete)
            return res.status(404).json({ message: "Fayl topilmadi" })
          await deleteFileFromServer(fileToDelete.path)
          await File.findByIdAndDelete(id)
          res.status(200).json({
            message: `${fileToDelete.name} fayl muvaffaqiyatli o'chirildi`,
          })
        } catch (e) {
          res.status(500).json({ message: e.message })
        }
        break
      }
      default: {
        res.status(405).json({ message: "Method noto`g`ri!" })
      }
    }
  } catch (e) {
    res.status(500).json(e)
  }
}

export default handler
