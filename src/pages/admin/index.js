import React from "react"
import AdminLayout from "../../layout/AdminLayout"

function Admin() {
  return <AdminLayout>Admin page!</AdminLayout>
}

export default Admin
