// src/pages/admin/file
import React, { useState, useEffect } from "react"
import AdminLayout from "../../../layout/AdminLayout"
import Table from "../../../components/Table"
import { toast } from "react-toastify"

function Index() {
  const url = "file"
  const [state, setState] = useState([])

  const fetchData = async () => {
    const r = await fetch(`/api/${url}`)
    if (r.ok) {
      const data = await r.json()
      setState(data)
    } else {
      console.error("Server bilan muammo", r.status)
      toast.error(r.message || "Server bilan muammo: ", r.status)
    }
  }

  useEffect(() => {
    fetchData().catch((e) => console.error("Xatolik yuz berdi", e.message))
  }, [])

  return (
    <AdminLayout>
      <Table states={state} url={url} />
    </AdminLayout>
  )
}

export default Index
