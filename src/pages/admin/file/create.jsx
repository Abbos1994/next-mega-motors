// pages/create.js
import React, { useState } from "react"
import { useRouter } from "next/router"
import { toast } from "react-toastify"
import AdminLayout from "../../../layout/AdminLayout"

const Create = () => {
  const {push, query: { url }} = useRouter()
  const [file, setFile] = useState(null)

  const onSubmit = async (e) => {
    e.preventDefault()
    if (!file) return

    try {
      const data = new FormData()
      data.set("upload", file)
      const res = await fetch(`/api/${url}`, {
        method: "POST",
        body: data,
      })
      const { message } = await res.json()
      await toast.success(message || "Yuklandi!")
      return push("/admin/file")
    } catch (e) {
      toast.error(e.message)
    }
  }

  return (
    <AdminLayout>
      <h1>CREATE FILE</h1>
      <form onSubmit={onSubmit}>
        <input
          type="file"
          name="upload"
          onChange={(e) => setFile(e.target.files?.[0])}
        />
        <button
          className="text-green-400 hover:text-green-600 hover:underline font-bold uppercase p-3"
          type="submit"
        >
          Yuklash
        </button>
      </form>
    </AdminLayout>
  )
}

export default Create
