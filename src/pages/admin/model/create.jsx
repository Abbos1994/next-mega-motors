import React, { useState, useEffect } from "react"
import { useRouter } from "next/router"
import { toast } from "react-toastify"
import AdminLayout from "../../../layout/AdminLayout"
import Image from "next/image"

const Create = () => {
  const {
    push,
    query: { id, url },
  } = useRouter()
  const [state, setState] = useState({
    title: "",
    description: "",
    price: 0,
    text: "",
    favorite: false,
    img: "",
    brand: "",
  })
  const [img, setImg] = useState([])
  const [selectedImage, setSelectedImage] = useState(null)
  const [brands, setBrands] = useState([])
  useEffect(() => {
    getBrands()
    getImages()
    getModelById()
  }, [id])
  const handleImageChange = (e) => {
    const selectedImageDetails = img.find((i) => i._id === e.target.value)
    setState((prev) => ({ ...prev, img: e.target.value }))
    setSelectedImage(selectedImageDetails)
  }

  const getImages = async () => {
    try {
      const r = await fetch("/api/file")
      const i = await r.json()
      setImg(i)
    } catch (e) {
      toast.error(e.message)
    }
  }

  const getBrands = async () => {
    try {
      const r = await fetch("/api/brand")
      const d = await r.json()
      setBrands(d)
    } catch (error) {
      toast.error(error.message)
    }
  }
  async function getModelById() {
    if (id) {
      const r = await fetch(`/api/model?id=${id}`)
      const d = await r.json()
      await setState(d)
      await setSelectedImage(img.find((i) => i._id === d.img))
    }
  }

  const handleChange = (e) => {
    const { value, name, type, checked } = e.target

    if (type === "checkbox") {
      setState((prev) => ({ ...prev, [name]: checked }))
    } else {
      setState((prev) => ({ ...prev, [name]: value }))
    }
  }

  const onSubmit = async (e) => {
    e.preventDefault()
    if (!state.title || !state.img) return

    try {
      const urlPath = id ? `/api/${url}?id=${id}` : `/api/${url}`
      const config = {
        method: id ? "PUT" : "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(state),
      }

      const res = await fetch(urlPath, config)
      const { message } = await res.json()

      toast.success(message || "Operation Successful!")
      return push(`/admin/model`)
    } catch (error) {
      console.error(error)
      toast.error(error.message)
    }
  }

  return (
    <AdminLayout>
      <h1 className="text-3xl font-bold mb-6">
        {(id ? "update " : "create " + url).toUpperCase()}
      </h1>
      <form onSubmit={onSubmit} className="max-w-md mx-auto">
        <div className="mb-4">
          <label htmlFor="title" className="block text-sm font-semibold mb-2">
            Title:
          </label>
          <input
            type="text"
            id="title"
            name="title"
            value={state.title}
            onChange={handleChange}
            placeholder="Title"
            className="w-full border border-gray-300 rounded p-2"
          />
        </div>
        <div className="mb-4">
          <label
            htmlFor="description"
            className="block text-sm font-semibold mb-2"
          >
            Description:
          </label>
          <input
            type="text"
            id="description"
            name="description"
            value={state.description}
            onChange={handleChange}
            placeholder="Description"
            className="w-full border border-gray-300 rounded p-2"
          />
        </div>
        <div className="mb-4">
          <label htmlFor="price" className="block text-sm font-semibold mb-2">
            Price:
          </label>
          <input
            type="number"
            id="price"
            name="price"
            value={state.price}
            onChange={handleChange}
            placeholder="Price"
            className="w-full border border-gray-300 rounded p-2"
          />
        </div>
        <div className="mb-4">
          <label htmlFor="text" className="block text-sm font-semibold mb-2">
            Text:
          </label>
          <textarea
            id="text"
            name="text"
            value={state.text}
            onChange={handleChange}
            placeholder="Text"
            className="w-full border border-gray-300 rounded p-2"
          />
        </div>
        <div className="mb-4">
          <label
            htmlFor="favorite"
            className="text-sm font-semibold mb-2 cursor-pointer"
          >
            Favorite:
          </label>
          <input
            type="checkbox"
            id="favorite"
            name="favorite"
            checked={state.favorite}
            onChange={handleChange}
            className={state.favorite ? "hidden" : "cursor-pointer mx-3 "}
          />
          {state.favorite && (
            <span
              className={"cursor-pointer p-2"}
              onClick={() => setState({ ...state, favorite: !state.favorite })}
            >
              ⭐
            </span>
          )}
        </div>
        <div className="mb-4">
          <label htmlFor="brand" className="block text-sm font-semibold mb-2">
            Brand:
          </label>
          <select
            id="brand"
            name="brand"
            onChange={handleChange}
            className="w-full border border-gray-300 rounded p-2"
            value={state.brand}
          >
            <option value="" disabled>
              Select a brand
            </option>
            {brands.map(({ _id, title }) => (
              <option key={_id} value={_id}>
                {title}
              </option>
            ))}
          </select>
        </div>
        <div className="mb-4">
          <label htmlFor="img" className="block text-sm font-semibold mb-2">
            Select an image:
          </label>
          <select
            id="img"
            name="img"
            onChange={handleImageChange}
            className="w-full border border-gray-300 rounded p-2"
            defaultValue={state.img}
          >
            <option value="" disabled>
              Select a Image
            </option>
            {img.map(({ _id, name }) => (
              <option key={_id} value={_id}>
                {name}
              </option>
            ))}
          </select>
        </div>
        <button
          className="bg-green-400 hover:bg-green-600 font-bold py-2 px-4 rounded-full mb-4"
          type="submit"
        >
          {id ? "Update" : "Create"}
        </button>

        <hr />

        {selectedImage?.path && (
          <Image
            src={selectedImage.path}
            alt={selectedImage.name || "Image Alt Text"}
            width={selectedImage.size}
            height={selectedImage.size}
            className="rounded mt-4"
          />
        )}
      </form>
    </AdminLayout>
  )
}

export default Create
