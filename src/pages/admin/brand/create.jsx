import React, { useState, useEffect } from "react"
import { useRouter } from "next/router"
import { toast } from "react-toastify"
import AdminLayout from "../../../layout/AdminLayout"
import Image from "next/image"

const Create = () => {
  const {push, query: { id, url }} = useRouter()
  const [state, setState] = useState({ title: "", img: "" })
  const [img, setImg] = useState([])
  const [selectedImage, setSelectedImage] = useState(null)

  useEffect(() => {
    const getImages = async () => {
        const response = await fetch("/api/file")
        const images = await response.json()
        setImg(images)
    }

    getImages().catch(e => toast.error(e.message))

    if (id) {
      fetch(`/api/${url}?id=${id}`)
        .then((response) => response.json())
        .then(({title, img}) => {
          setState({ title, img})
          setSelectedImage(img)
        })
        .catch((e) => toast.error(e.message))
    }
  }, [id])

  const handleTitleChange = (e) => {
    setState((prev) => ({ ...prev, title: e.target.value }))
  }

  const handleImageChange = (e) => {
    const selectedImageDetails = img.find((i) => i._id === e.target.value)
    setState((prev) => ({ ...prev, img: e.target.value }))
    setSelectedImage(selectedImageDetails)
  }

  const onSubmit = async (e) => {
    e.preventDefault()
    if (!state.title || !state.img) return

    try {
      const u = id ? `/api/${url}?id=${id}` : `/api/${url}`
      const c = {
        method: id ? "PUT" : "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(state),
      }

      const res = await fetch(u, c)

      const { message } = await res.json()

      await toast.success(message || "Operation Successful!")
      return push(`/admin/brand`)
    } catch (e) {
      console.error(e)
      toast.error(e.message)
    }
  }

  return (
    <AdminLayout>
      <h1 className="text-3xl font-bold mb-6">
        {(id ? "update " : "create " + url).toUpperCase()}
      </h1>
      <form onSubmit={onSubmit} className="text-black max-w-md mx-auto">
        <div className="mb-4">
          <label htmlFor="title" className="block text-sm font-semibold mb-2">
            Title:
          </label>
          <input
            type="text"
            id="title"
            value={state.title}
            onChange={handleTitleChange}
            placeholder="Title"
            className="w-full border border-gray-300 rounded p-2"
          />
        </div>
        <div className="mb-4">
          <label htmlFor="img" className="block text-sm font-semibold mb-2">
            Select an image:
          </label>
          <select
            name="img"
            id="img"
            onChange={handleImageChange}
            className="w-full border border-gray-300 rounded p-2"
            defaultValue={!state.img ? "" : state.img}
          >
            {img.map(({ _id, name }) => (
              <option key={_id} value={_id}>
                {name}
              </option>
            ))}
          </select>
        </div>
        <button
          className="bg-green-400 hover:bg-green-600 text-white font-bold py-2 px-4 rounded-full"
          type="submit"
        >
          {id ? "Update" : "Create"}
        </button>
        {selectedImage?.path ? (
          <Image
            src={selectedImage.path}
            alt={selectedImage.name || "Image Alt Text"}
            width={selectedImage.size}
            height={selectedImage.size}
            className="rounded"
          />
        ) : (
          <span>No image available</span>
        )}
      </form>
    </AdminLayout>
  )
}

export default Create
