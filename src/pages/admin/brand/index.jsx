// src/pages/admin/brand
import React, { useState, useEffect } from "react"
import AdminLayout from "../../../layout/AdminLayout"
import Table from "../../../components/Table"
import { toast } from "react-toastify"

function Index() {
  const url = "brand"
  const [states, setStates] = useState([])

  const fetchData = async () => {
    const r = await fetch(`/api/${url}`)
    if (r.ok) {
      const data = await r.json()
      setStates(data)
    } else {
      console.error("Server bilan muammo", r.status)
      toast.error(r.message || "Server bilan muammo", r.status)
    }
  }

  useEffect(() => {
    fetchData().catch((e) => console.error("Xatolik yuz berdi", e.message))
  }, [])

  return (
    <AdminLayout>
      <Table states={states} url={url} />
    </AdminLayout>
  )
}

export default Index
