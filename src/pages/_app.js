import "@/styles/globals.css"
import { Provider } from "react-redux"
import store from "../store/store"
import { ToastContainer } from "react-toastify"
import "react-toastify/dist/ReactToastify.css"
import "@/styles/globals.css"

export default function App({ Component, pageProps }) {

  return (
    <Provider store={store}>
      <Component {...pageProps} />
      <ToastContainer
        position="bottom-center"
        autoClose={3000}
        // delay={2000}
        // toastStyle={{ backgroundColor: "grey" }}
        theme="dark"
      />
    </Provider>
  )
}
