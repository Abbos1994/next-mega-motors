import Link from "next/link"
import Navbar from "@/components/Navbar"
import Header from "@/components/Header"
import Footer from "@/components/Footer"

export default function Home() {
  return (
    <main>
      <Link href={"/admin/upload"}>Admin</Link>
      <Navbar />
      <Header />
      <Footer />
    </main>
  )
}
