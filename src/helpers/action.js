// src/actions/helpers.js
import api, { delApi, getApi, getOneApi, postApi, putApi } from "../api"
import { setLocalStore } from "./localStorage"

const endpointMap = {
  LOGIN: "login",
  USER: "user",
  USERS: "user",
  YOUTUBE: "youtube",
  YOUTUBES: "youtube",
  UPLOAD: "upload",
  UPLOADS: "upload",
  CLIENT: "client",
  CLIENTS: "client",
  RESULT: "result",
  RESULTS: "result",
  CATEGORY: "category",
  CATEGORYS: "category",
  CHILD: "child",
  CHILDS: "child",
  CONTENT: "content",
  CONTENTS: "content",
  DISCOUNT: "discount",
  DISCOUNTS: "discount",
  ROLE: "role",
  ROLES: "role",
}

const config = { headers: { "Content-Type": "multipart/form-data" } }

const getUrlFromType = (type) => {
  const endpoint = endpointMap[type.toUpperCase()]
  // if (!endpoint) throw new Error(`Unknown type: ${type}`)
  return endpoint + "/"
}

export const genericAction =
  (type, operation, data = null, id = null) =>
  async (dispatch) => {
    dispatch({ type: `${operation}_${type}_START` })
    const url = getUrlFromType(type)
    try {
      let responseData
      switch (operation) {
        case "LOGIN":
          responseData = await postApi(url, data)
          await setLocalStore(responseData.data.token)
          break
        case "CREATE_UPLOAD":
          responseData = await api.post(url, data, config)
          break
        case "FETCH":
          responseData = id ? await getOneApi(url, id) : await getApi(url)
          break
        case "CREATE":
          responseData = id
            ? await postApi(`${url + id}`, data)
            : await postApi(url, data)
          break
        case "UPDATE":
          if (!id) throw new Error("ID is required for UPDATE operation")
          responseData = await putApi(url, id, data)
          break
        case "DELETE":
          if (!id) throw new Error("ID is required for DELETE operation")
          await delApi(url, id)
          break
        default:
          throw new Error("Invalid operation")
      }
      dispatch({
        type: `${operation}_${type}_SUCCESS`,
        payload: responseData ? responseData.data : id,
      })
    } catch (error) {
      dispatch({
        type: `${operation}_${type}_FAIL`,
        payload: error.message,
      })
    }
  }
