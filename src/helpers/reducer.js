// src/reducers/reducer.js
export const createInitialState = (dataKey) => ({
  [dataKey]: [],
  [dataKey.slice(0, -1)]: null,
  error: null,
})
export const genericReducer = (type, state, action) => {
  switch (action.type) {
    case `FETCH_${type}S_START`:
    case `CREATE_${type}_START`:
    case `UPDATE_${type}_START`:
    case `DELETE_${type}_START`:
      return { ...state, error: null }
    case `FETCH_${type}S_SUCCESS`:
      return {
        ...state,
        [type.toLowerCase() + "s"]: action.payload,
      }
    case `FETCH_${type}S_FAIL`:
    case `CREATE_${type}_FAIL`:
    case `UPDATE_${type}_FAIL`:
    case `DELETE_${type}_FAIL`:
      return { ...state, error: action.payload }
    case `LOGIN_SUCCESS`:
      return {
        ...state,
        isAuthenticated: true,
        user: action.payload,
      }
    case `LOGIN_FAIL`:
      return { ...state, error: action.payload }
    case `LOGOUT`:
      return { ...state, user: null, isAuthenticated: false }
    case `FETCH_${type}_SUCCESS`:
      return { ...state, [type.toLowerCase()]: action.payload }
    case `CREATE_${type}_SUCCESS`:
      return {
        ...state,
        [`${type.toLowerCase()}s`]: [
          ...state[`${type.toLowerCase()}s`],
          action.payload,
        ],
      }
    case `UPDATE_${type}_SUCCESS`:
      return {
        ...state,
        [`${type.toLowerCase()}s`]: state[`${type.toLowerCase()}s`].map(
          (item) => (item._id === action.payload._id ? action.payload : item)
        ),
        loading: false,
      }
    case `DELETE_${type}_SUCCESS`:
      return {
        ...state,
        [`${type.toLowerCase()}s`]: state[`${type.toLowerCase()}s`].filter(
          (item) => item._id !== action.payload
        ),
      }
    default:
      return state
  }
}
