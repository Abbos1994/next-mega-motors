import React from "react"
import { baseURL } from "@/api"

export const tOption = {
  position: "bottom-center",
}

export const Media = (item) => {
  if (Array.isArray(item) && item.length > 0) {
    const firstItem = item[0].url
    return (
      <video
        width="320"
        height="240"
        className="h-32 w-32 transform rounded-lg object-cover shadow-md transition-transform hover:scale-110"
        controls
      >
        <source
          src={`${baseURL}${firstItem}`}
          type={`video/${firstItem.split(".").pop()}`}
        />
        Your browser does not support the video tag.
      </video>
    )
  }
  if (item.match(/\.(jpg|webp|jpeg|png)$/i)) {
    return (
      <img
        src={`${baseURL}${item}`}
        alt={item}
        className="h-32 w-32 transform rounded-lg object-cover shadow-md transition-transform hover:scale-110"
      />
    )
  } else {
    return (
      <video
        width="320"
        height="240"
        className="h-32 w-32 transform rounded-lg object-cover shadow-md transition-transform hover:scale-110"
        controls
      >
        <source
          src={`${baseURL}${item}`}
          type={`video/${item.split(".").pop()}`}
        />
        Your browser does not support the video tag.
      </video>
    )
  }
}
