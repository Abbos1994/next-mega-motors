// components/AdminLayout.js
import React from "react"
import Link from "next/link"

const AdminLayout = ({ children }) => {
  return (
    <div className="bg-gray-800 text-white min-h-screen flex">
      <nav className="w-64 bg-gray-900 p-4">
        <h1 className="text-2xl font-bold mb-6">
          <Link href={"/admin"}>MegaMotors</Link>
        </h1>
        <ul className="space-y-2">
          <li>
            <Link
              className="block hover:text-gray-300"
              href={"/admin/file"}
              passHref
            >
              Fayllar
            </Link>
          </li>
          <li>
            <Link
              className="block hover:text-gray-300"
              href={"/admin/brand"}
              passHref
            >
              Brendlar
            </Link>
          </li>
          <li>
            <Link
              className="block hover:text-gray-300"
              href={"/admin/model"}
              passHref
            >
              Modellar
            </Link>
          </li>
          <li>
            <Link
              className="block hover:text-gray-300"
              href={"/admin/sale"}
              passHref
            >
              Chegirmalar
            </Link>
          </li>
          <li>
            <Link
              className="block hover:text-gray-300"
              href={"/admin/question"}
              passHref
            >
              Savollar
            </Link>
          </li>
          <li>
            <Link
              className="block hover:text-gray-300"
              href={"/admin/youtube"}
              passHref
            >
              Youtube
            </Link>
          </li>
        </ul>
      </nav>
      <main className="flex-1 p-4">{children}</main>
    </div>
  )
}

export default AdminLayout
